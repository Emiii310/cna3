﻿using Generated;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chat
{
    public partial class MainWindow : Window
    {

        const string Host = "localhost";
        const int Port = 16842;

        Channel channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);

        LogInRequest client = new LogInRequest();
        ChatRequest clientMessage = new ChatRequest();
        bool isConnected = false;

        private AsyncDuplexStreamingCall<ChatRequest, ChatResponse> stream;

        public MainWindow()
        {
            InitializeComponent();
            UpdateChat();
        }

        private void AppendLineToChatBox(string message)
        {
            chatbox.Dispatcher.BeginInvoke(new Action<string>((messageToAdd) =>
            {
                chatbox.Inlines.Add(new Run(message.Substring(0, message.IndexOf(":") + 2)));

                message = message.Substring(message.IndexOf(":") + 2);

                string semn = "*_~'";
                string x = string.Empty;

                Regex rg;

                MatchCollection matchedAuthors;

                for (int index = 0; index < semn.Length; index++)
                {
                    if (message.Contains(semn[index].ToString()))
                    {
                        x = semn[index].ToString();
                    }
                }

                switch (x)
                {
                    case "*":
                        {
                            rg = new Regex(@"(^|\s)[*]([a-z]|[A-Z])+(\s([a-z]|[A-Z])+)*[*](\s|$)");

                            matchedAuthors = rg.Matches(message);

                            if (matchedAuthors.Count != 0)
                            {
                                string text = matchedAuthors[0].Value;

                                chatbox.Inlines.Add(new Run(message.Substring(0, message.IndexOf(text))));

                                for (int count = 0; count < matchedAuthors.Count; count++)
                                {
                                    text = matchedAuthors[count].Value;

                                    if (count == 0 || count == matchedAuthors.Count - 1)
                                    {
                                        text = text.Remove(text.IndexOf("*"), 1);

                                        if (text.Contains("*"))
                                        {
                                            text = text.Remove(text.LastIndexOf("*"), 1);
                                        }
                                    }
                                    chatbox.Inlines.Add(new Bold(new Run(text)));
                                }

                                int index = message.LastIndexOf(matchedAuthors[matchedAuthors.Count - 1].ToString()[matchedAuthors[matchedAuthors.Count - 1].ToString().Length - 1]) + 1;
                                chatbox.Inlines.Add(new Run(message.Substring(index)));
                            }
                            else
                            {
                                chatbox.Inlines.Add(new Run(message));
                            }
                            break;
                        }

                    case "_":
                        {
                            rg = new Regex(@"(^|\s)[_]([a-z]|[A-Z])+(\s([a-z]|[A-Z])+)*[_](\s|$)");

                            matchedAuthors = rg.Matches(message);

                            if (matchedAuthors.Count != 0)
                            {
                                string text = matchedAuthors[0].Value;

                                chatbox.Inlines.Add(new Run(message.Substring(0, message.IndexOf(text))));

                                for (int count = 0; count < matchedAuthors.Count; count++)
                                {
                                    text = matchedAuthors[count].Value;

                                    if (count == 0 || count == matchedAuthors.Count - 1)
                                    {
                                        text = text.Remove(text.IndexOf("_"), 1);

                                        if (text.Contains("_"))
                                        {
                                            text = text.Remove(text.LastIndexOf("_"), 1);
                                        }
                                    }
                                    chatbox.Inlines.Add(new Italic(new Run(text)));
                                }

                                int index = message.LastIndexOf(matchedAuthors[matchedAuthors.Count - 1].ToString()[matchedAuthors[matchedAuthors.Count - 1].ToString().Length - 1]) + 1;
                                chatbox.Inlines.Add(new Run(message.Substring(index)));
                            }
                            else
                            {
                                chatbox.Inlines.Add(new Run(message));
                            }
                            break;
                        }

                    case "'":
                        {
                            rg = new Regex(@"(^|\s)[']([a-z]|[A-Z])+(\s([a-z]|[A-Z])+)*['](\s|$)");

                            matchedAuthors = rg.Matches(message);

                            if (matchedAuthors.Count != 0)
                            {
                                string text = matchedAuthors[0].Value;

                                chatbox.Inlines.Add(new Run(message.Substring(0, message.IndexOf(text))));

                                for (int count = 0; count < matchedAuthors.Count; count++)
                                {
                                    text = matchedAuthors[count].Value;

                                    if (count == 0 || count == matchedAuthors.Count - 1)
                                    {
                                        text = text.Remove(text.IndexOf("'"), 1);

                                        if (text.Contains("'"))
                                        {
                                            text = text.Remove(text.LastIndexOf("'"), 1);
                                        }
                                    }
                                    chatbox.Inlines.Add(new Underline(new Run(text)));
                                }

                                int index = message.LastIndexOf(matchedAuthors[matchedAuthors.Count - 1].ToString()[matchedAuthors[matchedAuthors.Count - 1].ToString().Length - 1]) + 1;
                                chatbox.Inlines.Add(new Run(message.Substring(index)));
                            }
                            else
                            {
                                chatbox.Inlines.Add(new Run(message));
                            }
                            break;
                        }

                    default:
                        {
                            chatbox.Inlines.Add(new Run(message));
                            break;
                        }
                }

            }), new object[] { message });
        }

        private async Task UpdateChat()
        {
            var chat = new ChatService.ChatServiceClient(channel);

            try
            {
                using (stream = chat.chatStream())
                {
                    while (await stream.ResponseStream.MoveNext(CancellationToken.None))
                    {
                        var serverMessage = stream.ResponseStream.Current;

                        var displayMessage = string.Format("{0} says {1}{2}", serverMessage.Name, serverMessage.Textmessage, Environment.NewLine);

                        AppendLineToChatBox(displayMessage);
                    }
                }
            }
            catch (RpcException)
            {
                stream = null;
                throw;
            }
        }

        void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            var logIn = new LogInService.LogInServiceClient(channel);
            logIn.logOut(client);
        }


        private void btn_connect_Click(object sender, RoutedEventArgs e)
        {
            if (txt_name.Text.ToString() == "")
                MessageBox.Show("Ups! You forgot to set your name!", "Error!");
            else
            {
                if (isConnected == true)
                    MessageBox.Show("Ups! You are already connected!", "Error!");
                else
                {
                    client.Name = txt_name.Text.ToString();
                    var logIn = new LogInService.LogInServiceClient(channel);
                    logIn.logIn(client);

                    isConnected = true;

                    string message;

                    message = "You are now Connected! Welcome " + client.Name + "!";
                    lb_connectBool.Content = message;
                    lb_connectBool.Foreground = new SolidColorBrush(Colors.Green);
                }
            }
        }

        private async void btn_send_Click(object sender, RoutedEventArgs e)
        {
            if (isConnected == false)
            {
                MessageBox.Show("You are not connected!", "Error!");
            }
            else
            {
                if (txt_message.Text == "")
                {
                    MessageBox.Show("You can't send an empty message.", "Error!");
                }
                else
                {
                    clientMessage.Name = client.Name;
                    clientMessage.Textmessage = txt_message.Text;

                    var chat = new ChatService.ChatServiceClient(channel);
                    chat.sendMessage(clientMessage);

                    if (stream != null)
                    {
                        await stream.RequestStream.WriteAsync(clientMessage);
                    }
                    txt_message.Text = "";
                }
            }
        }
    }
}
